#!/usr/bin/env bash

work=15
work_option="@work"

get_tmux_option() {
	local -r option="${1}"
	local -r default_option="${2}"
	local -r option_value="$(tmux show-option -gqv "${option}")"
	if [ -z "${option_value}" ] ; then
		echo "${default_option}"
	else
		echo "${option_value}"
	fi
}

pomodoro_timer() {
	echo $$ >> /tmp/goldenapple.pid
	echo "running" > /tmp/goldenapple.status

	IFS=:
	set -- "$@"
	secs=$(( ${1#0} * 60 + 00 ))
	while [ ${secs} -gt 0 ]
	do
		sleep 1 &
		printf "\r %02d:%02d " $(( ( secs/60 ) % 60 )) $(( secs % 60 ))
		secs=$(( secs - 1 ))
		wait
	done
	echo
	for p in $(pgrep "${CURRENT_DIR}"/scripts/timer.sh | grep -v grep | awk '{print $2}') ; do
		kill -9 "${p}"
	done
	tmux source-file ~/.tmux.conf >/dev/null 2>&1
	tmux display-popup -w 30% -h 8% echo "pomodoro work period done"
}

main() {
	local -r work_count="$(get_tmux_option "${work_option}" "${work}")"
	pomodoro_timer "${work_count}"
}

main
