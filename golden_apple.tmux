#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

timer="#(${CURRENT_DIR}/scripts/timer.sh)"
interpolate_string="\#{goldenapple}"

interpolate() {
	local -r status="${1}"
	local -r status_value=$(tmux show-option -gqv "${status}")
	tmux set-option -gq "${status}" "${status_value/$interpolate_string/$timer}"
}

timer() {
	interpolate "status-left"
	interpolate "status-right"
}

killtimer() {
	for p in $(pgrep "${CURRENT_DIR}"/scripts/timer.sh | grep -v grep | awk '{print $2}') ; do
		kill -9 "${p}"
	done
	tmux source-file ~/.tmux.conf >/dev/null 2>&1
}

main() {
	tmux source-file ~/.tmux.conf >/dev/null 2>&1
	case "${1}" in
		start) timer ;;
		stop) killtimer ; exit 0 ;;
	esac
}

main "${1}"
