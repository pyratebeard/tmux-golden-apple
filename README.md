# tmux golden apple

A simple Pomodoro timer for your tmux statusline

## Installation
### With [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm) (recommended)
Add the plugin to your list of TPM plugins in `.tmux.conf`
```
set -g @plugin 'https://gitlab.com/pyratebeard/tmux-golden-apple.git'
```

Fetch the plugin and source it using `prefix + I`.

### Manual installation
Clone the repo
```
git clone https://gitlab.com/pyratebeard/tmux-golden-apple.git ~/.tmux/plugins/
```

Reload your Tmux environment by typing the following in the terminal
```
tmux source-file ~/.tmux.conf
```

## Usage
Add the format string `#{goldenapple}` in to your `status-right` or `status-left` option.

Add the following bind-key options
```
bind-key g run-shell "~/.tmux/plugins/tmux-golden-apple/golden_apple.tmux start"
bind-key G run-shell "~/.tmux/plugins/tmux-golden-apple/golden_apple.tmux stop"
```

By default the timer will run for 15 minutes.
You can modify the timer using the option `@work`, for example to set it for 30 minutes:
```
set -g @work 30
```

## License
[MIT](LICENSE)
